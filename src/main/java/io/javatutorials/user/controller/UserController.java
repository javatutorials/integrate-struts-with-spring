package io.javatutorials.user.controller;

import io.javatutorials.user.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created on 4/11/16.
 */
@Controller
@RequestMapping(path = "/users")
public class UserController {

    @RequestMapping(path = "/{userName}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable(value = "userName") String userName) {
        User user = new User();
        user.setUserName(userName);
        user.setAge(20);
        return user;
    }
}
