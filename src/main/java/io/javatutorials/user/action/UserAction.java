package io.javatutorials.user.action;

import io.javatutorials.user.bo.UserService;
import com.opensymphony.xwork2.ActionSupport;
 
public class UserAction extends ActionSupport{

	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String execute() throws Exception {

		userService.getUserName();
		
		return SUCCESS;
	}
}