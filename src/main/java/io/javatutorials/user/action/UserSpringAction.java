package io.javatutorials.user.action;

import io.javatutorials.user.bo.UserService;

public class UserSpringAction{

	//DI via Spring
	UserService userService;
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String execute() throws Exception {
		
		userService.getUserName();
		
		return "success";
		
	}
}